#include <iostream>
#include <cstdio>
#include <exception>

using namespace std;

class Excecao : public exception {
    const char * message;
    Excecao(){}
public:
    Excecao(const char * s) throw() : message(s){}
    const char * what() const throw() {return message;}
};

const Excecao e_ValorForaDosLimites("Valor fora dos limites");
const Excecao e_DadoInconsistente("Entrada nao reconhecida");

void funcao_com_erro(int tipo) {
    cout << "Funcao com problemas: " << tipo << endl;
    switch (tipo) {
        case 1:
            throw (exception());
            break;
        case 2:
            throw Excecao("Erro: Excecao Personalizada");
            break;
        case 3:
            throw e_DadoInconsistente;
            break;
        default:
            break;
    }
}

int main( int argc, char ** argv ) {
    
    cout << "Teste de Excecao!" << endl;
    
    try {
        funcao_com_erro(1);
    }
    catch (exception & e) {
        cout << "Excecao " << e.what() << endl;
    }
    
    try {
        funcao_com_erro(2);
    }
    catch (Excecao & e) {
        cout << "Excecao " << e.what() << endl;
    }

    try {
        funcao_com_erro(3);
    }
    catch (Excecao & e) {
        cout << "Excecao " << e.what() << endl;
    }
    
    return 0;
}